#!/usr/bin/env python
import os
import requests
try:
    import json
except ImportError:
    import simplejson as json


consul_token=os.environ.get("CONSUL_TOKEN")
type = os.environ.get("TYPE")
consul_address= "consul.bluebrick.work"
templist=dict()

def get_hosts():
    r = requests.get(url = "http://" + consul_address + "/v1/catalog/nodes",
                     headers = {'X-Consul-Token': consul_token}).json()
    for i in r:
        if type != "all":
            if i['Meta']['type'] == type:
                templist[i['Node']] = dict()
                templist[i['Node']]["ip"] = i['Address']
                for key, value in i['Meta'].items():
                    if key =="username":
                        templist[i['Node']]["ssh_user"] = value
                    else:
                        templist[i['Node']]["ssh_user"] = "missing_user"
        else:
            templist[i['Node']] = dict()
            templist[i['Node']]["ip"] = i['Address']
            for key, value in i['Meta'].items():
                if key == "username":
                    templist[i['Node']]["ssh_user"] = value
                else:
                    templist[i['Node']]["ssh_user"] = "missing_user"

    return templist

x=get_hosts()
data = {}
data['group'] = {'hosts':[],'vars':{}}
data['_meta'] = {'hostvars':{}}
for key, value in x.items():
    data['group']['hosts'].append(key)
    data['_meta']['hostvars'][key] = {"ansible_host": value['ip'] ,"ansible_ssh_user": value['ssh_user'] }

def main():
    print(json.dumps(data, sort_keys=True, indent=2))

if __name__ == '__main__':
    main()
